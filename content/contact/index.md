Ecodorp Klein Oers is een Sociocratische Vereniging en is voortgekomen uit de [Stichting Offgrid Ecovillages](https://offgridecovillages.org/).

Toekomstig adres van het dorp:\
Eindhovensebaan\
5505 JA  Veldhoven

Het dorp is nog niet gebouwd daarom bereik je ons nu het beste per e-mail: info@kleinoers.nl

