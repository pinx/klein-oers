Het dorp
--------

De gemeente Veldhoven heeft voor ons een kavel gereserveerd van 1
hectare in de wijk Zilverackers, te midden van andere CPO-projecten. Zo
wordt [Boserf](https://boserf.nl/) onze directe buur waarmee wij ook een
klein voedselbosje zullen gaan delen. 

Zilverackers ligt vlakbij het dorp Oerle.

### Klein Oerle?

Nee, Klein Oers. De lokale bevolking spreekt Oerle uit als Oers. Vandaar
de naam Klein Oers *(Klên Oers)*.

### De woningen

Ons wijkje zal bestaan uit:

-   5 grote langgevelhoeves
-   een dorpshuis
-   een centrale permacultuurtuin

In elke hoeve komen 8 woningen die bestaan uit:

-   1 of 2 persoons levensloopbestendige woningen op de begane grond
-   1 tot 3 persoons appartementen
-   2 á 3 persoons tussenwoningen
-   3 á 4 persoons gezins-hoekwoningen

Wonen met een kleine ecologische voetafdruk vraagt om kleine woningen.
Het woonoppervlak zal daarom variëren van 50 m² voor één of tweepersoons
huishoudens tot maximaal 92 m² voor een groot gezin.

Samengestelde gezinnen of mensen die hun ouders willen laten inwonen,
ook daar gaan we aan denken. Tussenmuren van houtskeletwoningen zijn
relatief eenvoudig door te breken om een deur te kunnen plaatsen. Op die
manier heb je twee aparte woningen maar is er wel van binnenuit
verbinding.

### Materialen

De bouwmethode ligt inmiddels vast: houtskelet in combinatie met
strobalen, aan de binnenkant afgewerkt met leem uit de directe omgeving.
De gevelbekleding aan de buitenkant maken we van hout. Deze bouwmethode
is in tegenstelling tot beton en baksteen CO~2~-negatief. Ook zal er bij
de keuze van overige bouwmaterialen rekening worden gehouden met het
milieu. Ademend en vochtregulerend voor een aangenaam en gezond
binnenklimaat. Met de duurzaamheid zit het ook wel goed. Wist je dat er
stobalenhuizen bestaan van meer dan honderd jaar oud?

### Speelse en ronde vormen/ de guldensnede

In de natuur is eigenlijk niets recht. In onze tuin willen we daarom
zoveel mogelijk gebruik maken van natuurlijke en speelse vormen. Dit
zorgt voor een fijne sfeer.

### Voor iedere portemonnee

Er komen zowel sociale huurwoningen als koopwoningen. We streven daarbij
de koopwoningen voor een groot deel binnen de sociale koopgrens te
houden. 

### Groen wijkje

Het idee is om de privétuintjes af te schermen met groene en liefst
eetbare 'schuttingen'. Denk bijvoorbeeld aan klimframbozen. Ook zal er
een gezamenlijke moestuin zijn met kas volgens het permacultuurprincipe.
We willen ook graag kippen en wellicht andere dieren. Zij mogen op een
voor hen zo prettig mogelijke manier rondscharrelen. Volledig
zelfvoorzienend voor de voedselvoorziening, daarvoor zijn we veel te
klein. Maar we vinden het wel een genot om zoveel mogelijk eetbaars van
ons kleine stukje grond te gaan halen. Daarnaast willen we heel graag
samenwerken met biologische boeren in de regio.

### Sociaal

Het dorp krijgt een sociaal en gezellig karakter. Te vergelijken met hoe
buren in vroegere tijden samen leefden en voor elkaar zorgden. Ons
ecodorp is geen commune waarin alles strak geregeld is. Er is vrijheid.
Ieder heeft zijn eigen levensvisie en dat respecteren we. Wel delen we
kernwaarden als ecologisch, duurzaam,  en gezond leven. In het dorpshuis
zullen diverse activiteiten worden georganiseerd, door ons zelf maar ook
door anderen van buiten het ecodorp.

### Sociocratie

Binnen het dorp is er sprake van een [sociocratische
besluitvorming](https://www.consentmethode.nl/).


Voorzieningen
-------------

Het dorpshuis is gepositioneerd aan de Eindhovense baan, welke in het
gebiedsplan 'De Zilverackers' gedefinieerd is als ondernemerslint. Het
heeft een lokale positie binnen het plan en vormt als het ware het
'gezicht' van het Ecodorp. Het voorziet in gedeelde functies zoals:
wasruimte, werkruimten, gezamenlijke woonkamer, keuken en logeerkamers.
Ook heeft het een centrale functie als het gaat om voorlichting én 'off
grid' energievoorzieningen. Het gebouw zal worden gerealiseerd met een
grote mate van zelfwerkzaamheid en zal zoveel mogelijk worden opgebouwd
uit hergebruikte
bouwmaterialen.

In totaal komen er 28 bergingen voor privé- en/of dubbelgebruik,
ondergebracht aan de kopgevels van de woningen. In de collectieve
binnentuin is nog eens ruimte voor 12 bergingen al dan niet met elkaar
verbonden voor collectief gebruik. De extra ruimte in de bergingen in de
collectieve binnentuin voorziet in ruimte voor opslag van
tuingereedschappen en ruimte om te klussen. Aan de oostzijde van de twee
parkeerclusters komen collectieve (overdekte) fietsenstalling(en). Deze
kunnen gebruikt worden door zowel bewoners als
bezoekers.

Bij aanvang leggen we een 'Smart Grid' aan. Het energieconcept
anticipeert op een off grid-situatie (in de toekomst). De energie wordt
lokaal opgewekte middels zonnepanelen op de verschillende daken
(woningen, dorpshuis en parkeerclusters). Uitgangspunt is om de
energievraag zo veel mogelijk te beperken. Energie besparen we door
zuinige apparaten, gezamenlijk te koken, vriezers te delen
etc.

De collectieve binnentuin wordt onder andere voorzien van wadi's,
bomen en een moestuin. Doel is om zoveel mogelijk water op het terrein
'vast te houden'. Er is aandacht voor een klimaatvriendelijke omgeving,
de tuin is klimaatrobuust. Er zijn plekken voorzien waar ontmoeting
wordt gestimuleerd. De tuin wordt met oog voor de natuur en het milieu
ingericht, waardoor biodiversiteit wordt bevorderd. Samen met de
bewoners van het naastgelegen Boserf beheren wij een klein
voedselbosje.

Uitgangspunt is het gebruik van droogtoiletten. Hierdoor wordt alleen
al ca. 40% drinkwater bespaard. Het water wat we gebruiken gaat
gezuiverd terug kan naar de aarde door middel van infiltratie. Het
grijze afvalwater van woonblok B, C, D en E wordt gezuiverd door het
Traiselect-systeem. Het zwarte afvalwater van het dorpshuis wordt
gezuiverd door een helofytenfilter. De tuin voorziet in waterbuffers ten
behoeve van intensieve regenval. Regenwater van woonblok A, het
dorpshuis en het parkeerdak wordt opgevangen en gebruikt als B-water tbv
de gedeelde wasmachines en de toiletspoeling in het
dorpshuis.

Het dorpshuis is gepositioneerd aan de Eindhovense baan, welke in het
gebiedsplan 'De Zilverackers' gedefinieerd is als ondernemerslint. Het
heeft een lokale positie binnen het plan en vormt als het ware het
'gezicht' van het Ecodorp. Het voorziet in gedeelde functies zoals:
wasruimte, werkruimten, gezamenlijke woonkamer, keuken en logeerkamers.
Ook heeft het een centrale functie als het gaat om voorlichting én 'off
grid' energievoorzieningen. Het gebouw zal worden gerealiseerd met een
grote mate van zelfwerkzaamheid en zal zoveel mogelijk worden opgebouwd
uit hergebruikte
bouwmaterialen.

In totaal komen er 28 bergingen voor privé- en/of dubbelgebruik,
ondergebracht aan de kopgevels van de woningen. In de collectieve
binnentuin is nog eens ruimte voor 12 bergingen al dan niet met elkaar
verbonden voor collectief gebruik. De extra ruimte in de bergingen in de
collectieve binnentuin voorziet in ruimte voor opslag van
tuingereedschappen en ruimte om te klussen. Aan de oostzijde van de twee
parkeerclusters komen collectieve (overdekte) fietsenstalling(en). Deze
kunnen gebruikt worden door zowel bewoners als
bezoekers.
