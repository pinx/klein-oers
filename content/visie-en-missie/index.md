### Visie en Missie Sociocratische Vereniging Ecodorp Klein Oers.

De mensheid leeft in harmonie met elkaar en neemt gezamenlijk
verantwoordelijkheid voor het voortbestaan van de aarde en alle
levensvormen die haar bewonen.

Onze missie is daarbij te laten zien dat het mogelijk is dat mensen
kunnen leven, wonen en werken in een omgeving die in balans is met de
natuur en elkaar.

### Doelstellingen

-   Het te bouwen ecodorp moet deels zelfvoorzienend, energieneutraal/
    C0~2~ negatief, duurzaam en ecologisch verantwoord zijn.
-   Opdoen en uitdragen van kennis en ervaring over het ontwikkelen van
    duurzame-, Off Grid-, energieneutrale, klimaatadaptieve, ecologisch
    verantwoorde, toekomstbestendige wooneenheden.
-   Laten zien aan gemeenten, woningbouwstichtingen en kopers, dat
    duurzaam en ecologisch wonen mogelijk is voor een brede groep
    mensen.
-   Streven naar en waarborgen van een samenleving waarbij sprake is van
    sociale samenhang en betrokkenheid tussen de bewoners met respect en
    zorg voor elkaar, elkaars leefwijze, de omgeving en het milieu.
-   Laten zien dat het mogelijk is om zelfvoorzienend te zijn en gebruik
    te maken van een circulaire economie waarbij alle middelen optimaal
    gebruikt worden en er nauwelijks sprake hoeft te zijn van afval en/
    of uitputting van natuurlijke bronnen.
-   Het samenbrengen van ecodorpbewoners die de doelstellingen van onze
    stichting onderschrijven.
-   Voorlichting geven over hoe ieder voor zich, in welke woonsituatie
    dan ook, kan bijdragen aan het herstel van "moeder aarde".
-   Projecten ontwikkelen of bestaande projecten implementeren om steden
    te vergroenen.
-   Rondleidingen. festivals en presentaties geven om mensen te laten
    zien en ervaren dat leven in harmonie met de natuur verrijkend is,
    ook al zul je concessies moeten doen.
-   Het ondersteunen van soortgelijke projecten door het delen van
    kennis en ervaring.
-   Groene activiteiten organiseren die tevens de sociale cohesie
    bevorderen.
-   Nieuwe high tech-ontdekkingen op het gebied van duurzaamheid een
    plek geven in de praktijk (in het ecodorp). Actieve samenwerking met
    universiteiten en hogescholen aangaan.
